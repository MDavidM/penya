@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Grupo de usuarios en sesión</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Role</th>
            <th>Operaciones</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>
            <td>{{ $user->cantidad }}</td>
            <td>

            </td>
          </tr>
          @empty
          <tr><td colspan="4">No hay usuarios!!</td></tr>
          @endforelse
        </tbody>
      </table>

      <a class="btn btn-primary" href="/groups/flush">Vacíar lista</a>

    </div>
  </div>
</div>
@endsection
